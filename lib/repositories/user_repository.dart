import 'package:dashboard/models/user.dart';
import 'package:dashboard/repositories/dbhelper.dart';
import 'package:dashboard/screens/component/snack_bar.dart';
import 'package:sqflite/sqflite.dart';

class UserRepository {
  static Future<List<User>> getAllUsers() async {
    final results = await db.query(DBHelper.userTable);
    List<User> users = List();

    for (final el in results) {
      final user = User.fromMapObject(el);
      users.add(user);
    }
    return users;
  }

  static Future<User> getUser(int id) async {
    final sql = '''SELECT * FROM ${DBHelper.userTable}
    WHERE ${DBHelper.id} = ?''';

    List<dynamic> params = [id];
    final data = await db.rawQuery(sql, params);

    final user = User.fromMapObject(data.first);
    return user;
  }

  static Future<void> addUser(User user) async {
    final result = await db.insert(DBHelper.userTable, user.toMap(),
        conflictAlgorithm: ConflictAlgorithm.fail);
    DBHelper.dbLogging(
        'Add user', 'insert', null, result, [user.toMap()]);
  }

  static Future<void> deleteUser(User user) async {
    final result = await db.delete(DBHelper.userTable,
        where: 'id = ?', whereArgs: [user.id]);

    DBHelper.dbLogging('Delete user', 'delete', null, result, [user.id]);
  }

  static Future<void> updateUser(User user) async {
    final result = await db.update(DBHelper.userTable, user.toMap(),
        where: 'id = ?',
        whereArgs: [user.id],
        conflictAlgorithm: ConflictAlgorithm.replace);

    DBHelper.dbLogging(
        'Update user', 'update', null, result, [user.toMap()]);
  }

}
