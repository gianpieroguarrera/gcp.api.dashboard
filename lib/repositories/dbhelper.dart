import 'dart:async';
import 'dart:io';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

Database db;

class DBHelper {
  static const dbName = 'testDB2';

  static const userTable = 'users';
  static const id = 'id';
  static const name = 'name';
  static const surname = 'surname';
  static const address = 'address';
  static const city = 'city';
  static const email = 'email';

  static void dbLogging(String functionName, String sql,
      [List<Map<String, dynamic>> selectQueryResult,
      int insertAndUpdateQueryResult,
      List<dynamic> params]) {
    print(functionName);
    print(sql);
    if (params != null) {
      print(params);
    }
    if (selectQueryResult != null) {
      print(selectQueryResult);
    } else if (insertAndUpdateQueryResult != null) {
      print(insertAndUpdateQueryResult);
    }
  }

  Future<void> createUserTable(Database db) async {
    final todoSql = '''CREATE TABLE $userTable (
      $id INTEGER PRIMARY KEY,
      $name TEXT,
      $surname TEXT UNIQUE,
      $address TEXT,
      $city TEXT,
      $email TEXT UNIQUE)''';

    await db.execute(todoSql);
  }

  Future<String> getDatabasePath(String dbName) async {
    final databasePath = await getDatabasesPath();
    final path = join(databasePath, dbName);

    //make sure the folder exists otherwise create it
    if (!await Directory(dirname(path)).exists()) {
      await Directory(dirname(path)).create(recursive: true);
    }
    return path;
  }

  Future<void> initDatabase() async {
    final path = await getDatabasePath(dbName);
    db = await openDatabase(path, version: 1, onCreate: onCreate);
    print(db);
  }

  Future<void> onCreate(Database db, int version) async {
    await createUserTable(db);
  }
}
