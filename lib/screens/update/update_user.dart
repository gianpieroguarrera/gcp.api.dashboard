import 'package:dashboard/screens/component/snack_bar.dart';
import 'package:dashboard/screens/view/view_user.dart';
import 'package:flutter/material.dart';
import 'package:dashboard/models/user.dart';
import 'package:dashboard/repositories/user_repository.dart';

class Update extends StatefulWidget {
  Update(this.user);

  final User user;

  @override
  _UpdateState createState() => _UpdateState();
}

class _UpdateState extends State<Update> {
  final _formKey = GlobalKey<FormState>();
  Future<List<User>> future;
  String name = '';
  String surname = '';
  String address = '';
  String city = '';
  String email = '';
  int id;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Modifica dati utente'),
          backgroundColor: Colors.black,
          leading: IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => View()),
                (Route<dynamic> route) => false,
              );
            },
          ),
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background.jpg"),
              fit: BoxFit.fill,
            ),
          ),
          child: ListView(
            padding: EdgeInsets.all(8),
            children: <Widget>[
              Form(
                  key: _formKey,
                  child: Column(children: <Widget>[
                    buildNameTextFormField(widget.user.name),
                    Divider(),
                    buildSurnameTextFormField(widget.user.surname),
                    Divider(),
                    buildAddressTextFormField(widget.user.address),
                    Divider(),
                    buildCityTextFormField(widget.user.city),
                    Divider(),
                    buildEmailTextFormField(widget.user.email),
                    Divider(),
                  ])),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      minimumSize: const Size(200, 50),
                      maximumSize: const Size(200, 50),
                    ),
                    onPressed: () => _updateUser(widget.user),
                    child: Text('Aggiorna Dati',
                        style: TextStyle(
                          fontSize: 17.0, // insert your font size here
                        )),
                  ),
                ],
              ),
            ],
          ),
        ));
  }

  void _updateUser(User user) async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      User userModify = new User(user.id, user.name, user.surname, user.address,
          user.city, user.email);
      if (!name.isEmpty) {
        userModify.name = name;
      }
      if (!surname.isEmpty) {
        userModify.surname = surname;
      }
      if (!address.isEmpty) {
        userModify.address = address;
      }
      if (!city.isEmpty) {
        userModify.city = city;
      }
      if (!email.isEmpty) {
        userModify.email = email;
      }
      await UserRepository.updateUser(userModify)
          .then((value) => Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => View()),
                (Route<dynamic> route) => false,
              ))
          .onError((error, stackTrace) =>
              showSnackBar(context, 'Errore modifica utente'));
    }
  }

  TextFormField buildNameTextFormField(String text) {
    return TextFormField(
      decoration: InputDecoration(
        fillColor: Colors.grey[300].withOpacity(0.7),
        filled: true,
      ),
      initialValue: text,
      onSaved: (value) => name = value,
    );
  }

  TextFormField buildSurnameTextFormField(String text) {
    return TextFormField(
      decoration: InputDecoration(
        fillColor: Colors.grey[300].withOpacity(0.7),
        filled: true,
      ),
      initialValue: text,
      onSaved: (value) => surname = value,
    );
  }

  TextFormField buildAddressTextFormField(String text) {
    return TextFormField(
      decoration: InputDecoration(
        fillColor: Colors.grey[300].withOpacity(0.7),
        filled: true,
      ),
      initialValue: text,
      onSaved: (value) => address = value,
    );
  }

  TextFormField buildCityTextFormField(String text) {
    city = text;
    return TextFormField(
      decoration: InputDecoration(
        fillColor: Colors.grey[300].withOpacity(0.7),
        filled: true,
      ),
      initialValue: text,
      onSaved: (value) => city = value,
    );
  }

  TextFormField buildEmailTextFormField(String text) {
    return TextFormField(
      validator: validateEmail,
      decoration: InputDecoration(
        fillColor: Colors.grey[300].withOpacity(0.7),
        filled: true,
      ),
      initialValue: text,
      onSaved: (value) => email = value,
    );
  }

  String validateEmail(String value) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return "Email is required";
    } else if (!regExp.hasMatch(value)) {
      return "Invalid Email";
    } else {
      return null;
    }
  }
}
