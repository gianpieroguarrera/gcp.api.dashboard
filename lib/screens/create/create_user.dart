import 'package:dashboard/screens/component/snack_bar.dart';
import 'package:flutter/material.dart';
import 'package:dashboard/models/user.dart';
import 'package:dashboard/repositories/user_repository.dart';

class Create extends StatefulWidget {
  Create({Key key, this.surname}) : super(key: key);

  final String surname;

  @override
  _CreateState createState() => _CreateState();
}

class _CreateState extends State<Create> {
  final _formKey = GlobalKey<FormState>();
  Future<List<User>> future;
  String name = 'No Name';
  String surname = 'No Surname';
  String address = 'No Address';
  String city = 'No City';
  String email = 'No Email';
  int id;

  @override
  initState() {
    super.initState();
    future = UserRepository.getAllUsers();
  }


  void _createUser() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      name = name != '' ? name : 'No Name';
      surname = surname != '' ? surname : 'No Surname';
      address = address != '' ? address : 'No Address';
      city = city != '' ? city : 'No City';
      email = email != '' ? email : 'No Email';
      final user = User(null, name, surname, address, city, email);
      await UserRepository.addUser(user).then((value) => showSnackBar(context, 'Utente registrato con successo!'))
                                        .onError((error, stackTrace) => showSnackBar(context,'Errore inserimento nuovo utente'));
      setState(() {
        id = user.id;
        future = UserRepository.getAllUsers();
      });
      print(user.id);
    }
  }


  TextFormField buildNameTextFormField() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: 'name',
        fillColor: Colors.grey[300].withOpacity(0.7),

        filled: true,
      ),
      onSaved: (value) => name = value,
    );
  }

  TextFormField buildSurnameTextFormField() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: 'surname',
        fillColor: Colors.grey[300].withOpacity(0.7),
        filled: true,
      ),
      onSaved: (value) => surname = value,
    );
  }

  TextFormField buildAddressTextFormField() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: 'address',
        fillColor: Colors.grey[300].withOpacity(0.7),
        filled: true,
      ),
      onSaved: (value) => address = value,
    );
  }

  TextFormField buildCityTextFormField() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: 'city',
        fillColor: Colors.grey[300].withOpacity(0.7),
        filled: true,
      ),
      onSaved: (value) => city = value,
    );
  }

  TextFormField buildEmailTextFormField() {
    return TextFormField(
      validator: validateEmail,
      decoration: InputDecoration(
        hintText: 'email',
        fillColor: Colors.grey[300].withOpacity(0.7),
        filled: true,
      ),
      onSaved: (value) => email = value,
    );
  }

  String validateEmail(String value) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return "Email is required";
    } else if (!regExp.hasMatch(value)) {
      return "Invalid Email";
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text('Aggiungi Utente'),
      ),
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background.jpg"),
              fit: BoxFit.fill,
            ),
          ),
          child: ListView(
        padding: EdgeInsets.all(8),
        children: <Widget>[
          Form(
              key: _formKey,
              child: Column(children: <Widget>[
                buildNameTextFormField(),
                Divider(),
                buildSurnameTextFormField(),
                Divider(),
                buildAddressTextFormField(),
                Divider(),
                buildCityTextFormField(),
                Divider(),
                buildEmailTextFormField(),
                Divider(),
              ])),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    minimumSize: const Size(200, 50),
                    maximumSize: const Size(200, 50),
                  ),
                onPressed: _createUser,
                child: Text('Create User',
                    style: TextStyle(
                      fontSize: 17.0, // insert your font size here
                    )),
              ),
            ],
          ),
        ],
      ),
    )
    );
  }


}