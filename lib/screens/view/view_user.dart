import 'dart:convert';

import 'package:dashboard/screens/component/snack_bar.dart';
import 'package:dashboard/screens/screen1/screen1.dart';
import 'package:dashboard/screens/update/update_user.dart';
import 'package:flutter/material.dart';
import 'package:dashboard/models/user.dart';
import 'package:dashboard/repositories/user_repository.dart';
import 'package:http/http.dart' as http;

class View extends StatefulWidget {
  View({Key key, this.surname}) : super(key: key);

  final String surname;

  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends State<View> {
  Future<List<User>> future;
  String name = 'No Name';
  String surname = 'No Surname';
  String address = 'No Address';
  String city = 'No City';
  String email = 'No Email';
  int id;

  @override
  initState() {
    super.initState();
    future = UserRepository.getAllUsers();
  }

  _deleteUser(User user) async {
    await UserRepository.deleteUser(user)
        .then(
            (value) => showSnackBar(context, 'Utente eliminato con successo!'))
        .onError((error, stackTrace) =>
            showSnackBar(context, 'Errore cancellazione utente'));
    setState(() {
      id = null;
      future = UserRepository.getAllUsers();
    });
  }

  Card buildItem(User user) {
    return Card(
      color: Colors.white.withOpacity(0.2),
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'name: ${user.name}',
              style: TextStyle(fontSize: 20),
            ),
            Text(
              'surname: ${user.surname}',
              style: TextStyle(fontSize: 20),
            ),
            Text(
              'address: ${user.address}',
              style: TextStyle(fontSize: 20),
            ),
            Text(
              'city: ${user.city}',
              style: TextStyle(fontSize: 20),
            ),
            Text(
              'email: ${user.email}',
              style: TextStyle(fontSize: 20),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Tooltip(
                    message: "Modifica i dati dell'utente",
                    child: TextButton.icon(
                      label: Text(''),
                      onPressed: () => Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (context) => Update(user)),
                        (Route<dynamic> route) => false,
                      ),
                      icon: Icon(
                        Icons.update,
                        color: Colors.white,
                      ),
                    )),
                Tooltip(
                    message: "Invia dati utente al server",
                    child: TextButton.icon(
                      label: Text(''),
                      onPressed: () => _sendUser(user),
                      icon: Icon(
                        Icons.web,
                        color: Colors.black,
                      ),
                    )),
                Tooltip(
                    message: "Cancella l'utente",
                    child: TextButton.icon(
                        label: Text(''),
                        onPressed: () => _deleteUser(user),
                        icon: Icon(Icons.delete, color: Colors.red))),
              ],
            )
          ],
        ),
      ),
    );
  }

  TextFormField buildNameTextFormField(User user) {
    return TextFormField(
      decoration: InputDecoration(
        hintText: user.name,
        fillColor: Colors.grey[300],
        filled: true,
      ),
      onSaved: (value) => name = value,
    );
  }

  TextFormField buildSurnameTextFormField(User user) {
    return TextFormField(
      decoration: InputDecoration(
        hintText: user.surname,
        fillColor: Colors.grey[300],
        filled: true,
      ),
      onSaved: (value) => surname = value,
    );
  }

  TextFormField buildAddressTextFormField(User user) {
    return TextFormField(
      decoration: InputDecoration(
        hintText: user.address,
        fillColor: Colors.grey[300],
        filled: true,
      ),
      onSaved: (value) => address = value,
    );
  }

  TextFormField buildCityTextFormField(User user) {
    return TextFormField(
      decoration: InputDecoration(
        hintText: user.city,
        fillColor: Colors.grey[300],
        filled: true,
      ),
      onSaved: (value) => city = value,
    );
  }

  TextFormField buildEmailTextFormField(User user) {
    return TextFormField(
      decoration: InputDecoration(
        hintText: user.email,
        fillColor: Colors.grey[300],
        filled: true,
      ),
      onSaved: (value) => email = value,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Text('Visualizza Storico Utenti'),
          leading: IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => Screen1()),
                (Route<dynamic> route) => false,
              );
            },
          ),
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background.jpg"),
              fit: BoxFit.fill,
            ),
          ),
          child: ListView(
            padding: EdgeInsets.all(8),
            children: <Widget>[
              FutureBuilder<List<User>>(
                future: future,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Column(
                        children: snapshot.data
                            .map((user) => buildItem(user))
                            .toList());
                  } else {
                    return SizedBox();
                  }
                },
              ),
            ],
          ),
        ));
  }

  _sendUser(User user) async {
    try {
      final response = await http.post(Uri.parse('https://urlserver'),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(user.toMap()));
      if (response.statusCode != 201) {
        showSnackBar(context, 'Failed to send user.');
        throw Exception('Failed to send user.');
      }
    } on Exception catch (_) {
      showSnackBar(context, 'Errore chiamata rest al servizio');
      print("Errore chiamata rest al servizio");
    }
  }
}
