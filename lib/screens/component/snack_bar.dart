import 'package:flutter/material.dart';

showSnackBar(BuildContext context, String message){
  SnackBar mySnackBar = SnackBar(
    content: Text('$message'),
    action: SnackBarAction(
        label: "CLOSE",
        textColor: Colors.white,
        onPressed: () {
          debugPrint('clicking on CLOSE');
        }),
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        side: BorderSide(color: Colors.amber, width: 5.0)),
    backgroundColor: Colors.blue,
    duration: Duration(milliseconds: 2000),
  );
  ScaffoldMessenger.of(context).showSnackBar(mySnackBar);
}