import 'package:flutter/material.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text("Dashboard",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: 'Open Sans', fontWeight: FontWeight.bold)),
            backgroundColor: Colors.black,
            leading: Padding(
                padding: const EdgeInsets.all(1.0),
                child: Image.asset(
                  "assets/images/spindox_white.png",
                  scale: 1.0,
                ))),
        backgroundColor: Colors.transparent,
        body: Stack(
          children: <Widget>[
            Container(
                alignment: Alignment(0, -0.5),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                    image: DecorationImage(
                  image: AssetImage('assets/images/background.jpg'),
                  fit: BoxFit.cover,
                ))),
            Positioned(
                width: MediaQuery.of(context).size.width,
                top: MediaQuery.of(context).size.width * 0.30,
                //TRY TO CHANGE THIS **0.30** value to achieve your goal
                child: Container(
                  margin: EdgeInsets.all(16.0),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset('assets/images/spindox.png', scale: 2.5),
                        SizedBox(
                          height: 20,
                        ),
                        Center(
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  minimumSize: const Size(200, 50),
                                  maximumSize: const Size(200, 50),
                                ),
                                onPressed: () {
                                  Navigator.pushNamed(context, "/create");
                                },
                                child: Text('Crea Nuovo Utente',
                                  style: TextStyle(
                                  fontSize: 17.0, // insert your font size here
                                ))
                              ),
                              Divider(),
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  minimumSize: const Size(200, 50),
                                  maximumSize: const Size(200, 50),
                                ),
                                onPressed: () {
                                  Navigator.pushNamed(context, "/view");
                                },
                                child: Text('Visualizza Utenti',
                                    style: TextStyle(
                                      fontSize: 17.0, // insert your font size here
                                    )),
                              ),
                            ]))
                      ]),
                ))
          ],
        ));
  }
}

