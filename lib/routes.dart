import 'package:dashboard/screens/create/create_user.dart';
import 'package:dashboard/screens/view/view_user.dart';
import 'package:flutter/widgets.dart';
import 'package:dashboard/screens/screen1/screen1.dart';

final Map<String, WidgetBuilder> routes = <String, WidgetBuilder>{
  "/": (BuildContext context) => Screen1(),
  "/create" : (BuildContext context) => Create(),
  "/view" : (BuildContext context) => View()
};
