import 'package:flutter/cupertino.dart';

class User {
  @required
  final int id;
  @required
  String name;
  @required
  String surname;
  @required
  String address;
  @required
  String city;
  @required
  String email;

  User(this.id, this.name, this.surname, this.address, this.city, this.email);

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'surname': surname,
      'address': address,
      'city': city,
      'email': email,
    };
  }

  User.fromMapObject(Map<String, dynamic> authorMap)
      : id = authorMap['id'],
        name = authorMap['name'],
        surname = authorMap['surname'],
        address = authorMap['address'],
        city = authorMap['city'],
        email = authorMap['email'];

  @override
  String toString() {
    return 'User{id: $id, name: $name, surname: $surname}';
  }
}
