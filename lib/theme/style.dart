import 'package:flutter/material.dart';

ThemeData appTheme() {
  return ThemeData(
      fontFamily: 'Roboto',
      primaryColor: Colors.deepOrangeAccent,
      accentColor: Colors.green,
      hintColor: Colors.white,
      buttonColor: Colors.greenAccent
  );
}