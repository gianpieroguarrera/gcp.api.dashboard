import 'package:flutter/material.dart';
import 'package:dashboard/repositories/dbhelper.dart';
import 'package:dashboard/routes.dart';
import 'package:dashboard/theme/style.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized(); //forse non serve per l'esempio
  await DBHelper().initDatabase();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Gestione Utenza',
      theme: appTheme(),
      initialRoute: '/',
      routes: routes,
    );
  }
}
